<?php

namespace App\Model\Data\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Model\UserManagement\Entities\User;

class Upload extends Model
{
    protected $table = 'uploads';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
