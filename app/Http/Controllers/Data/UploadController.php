<?php

namespace App\Http\Controllers\Data;

use Carbon\Carbon;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use App\Traits\FlashMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Data\Entities\Upload;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    use FlashMessage, UploadTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uploads     = Upload::orderBy('updated_at', 'DESC')->paginate(10);

        return view('uploads.index', ['uploads' => $uploads]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('uploads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $now            = 0;
        $dateFormat     = str_replace('-', '', $now);
        $countTable     = Upload::count();
        $sequence       = str_pad($countTable + 1, 0, '0', STR_PAD_LEFT);

        $upload             = new Upload();
        $upload->register   = 'REG - '.$dateFormat.$sequence;
        $upload->date       = $request->get('date');
        $upload->user_id    = $request->get('user');
        $upload->company    = $request->get('company');
        $upload->perumahan  = $request->get('perumahan');
        $upload->unit_total = $request->get('unit_total');
        $upload->ket        = $request->get('ket');
        if ($request->has('file')) {
            $file       = $request->file('file');
            $name       = 'REG - '.$dateFormat.$sequence;
            $folder     = '/uploads/';
            $filepath   = $folder.$name.'.'.$file->getClientOriginalExtension();
            $this->userUpload($file, $folder, 'public', $name);
            $upload->file = $filepath;
        }
        $upload->save();

        return redirect()->route('upload.create')->with(self::success('Data has been uploaded!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($upload_id)
    {
        $upload     = Upload::find($upload_id);

        return view('uploads.edit', ['upload' => $upload]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $upload_id)
    {
        $request->validate([
            'register'      => 'required|unique:uploads,register,'.$upload_id.',id',
            'date'          => 'required',
            'user'          => 'required',
            'company'       => 'required',
            'perumahan'     => 'required',
            'unit_total'    => 'required',
            'ket'           => 'required'
        ]);

        $upload             = Upload::find($upload_id);
        $upload->register   = $request->get('register');
        $upload->date       = $request->get('date');
        $upload->user_id    = $request->get('user');
        $upload->company    = $request->get('company');
        $upload->perumahan  = $request->get('perumahan');
        $upload->unit_total = $request->get('unit_total');
        $upload->ket        = $request->get('ket');
        if ($request->has('file')) {
            $this->userDeleteFileUpload($upload->file);
            $file       = $request->file('file');
            $name       = $upload->register;
            $folder     = '/uploads/';
            $filepath   = $folder.$name.'.'.$file->getClientOriginalExtension();
            $this->userUpload($file, $folder, 'public', $name);
            $upload->file = $filepath;
        }
        $upload->save();

        return redirect()->route('upload.edit', ['upload_id' => $upload->id])->with(self::success('Data has been uploaded!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($upload_id)
    {
        try {
            DB::beginTransaction();
            $upload     = Upload::query()->where('id', $upload_id)->firstOrFail();
            if (!empty($upload->file)) {
                $this->userDeleteFileUpload($upload->file);
            }
            $upload->delete();

            DB::commit();
            return redirect()->back()->with(self::warning('Data has been deleted!'));
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(self::danger('Data cannot deleted from database!'));
        }
    }
}
