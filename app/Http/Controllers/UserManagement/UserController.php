<?php

namespace App\Http\Controllers\UserManagement;

use App\Traits\FlashMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Model\UserManagement\Entities\User;

class UserController extends Controller
{

    use FlashMessage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users  = User::orderBy('updated_at', 'DESC')->paginate(10);

        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nik'       => 'required|unique:users,nik',
            'name'      => 'required',
            'email'     => 'required|unique:users,email',
            'phone'     => 'required',
            'password'  => 'required',
            'address'   => 'required'
        ]);

        $user           = new User();
        $user->nik      = $request->get('nik');
        $user->name     = $request->get('name');
        $user->email    = $request->get('email');
        $user->phone    = $request->get('phone');
        $user->password = Hash::make($request->get('password'));
        $user->address  = $request->get('address');
        $user->save();

        return redirect()->route('user.create')->with(self::success('Employee has been added!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        $user   = User::find($user_id);

        return view('users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        $request->validate([
            'nik'       => 'required|unique:users,nik,'.$user_id.',id',
            'name'      => 'required',
            'email'     => 'required|unique:users,email,'.$user_id.',id',
            'phone'     => 'required',
            'address'   => 'required'
        ]);

        $user           = User::find($user_id);
        $user->nik      = $request->get('nik');
        $user->name     = $request->get('name');
        $user->email    = $request->get('email');
        $user->phone    = $request->get('phone');
        $user->address  = $request->get('address');
        $user->save();

        return redirect()->route('user.edit', ['user_id' => $user->id])->with(self::success('Data employee has beed updated!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        try {
            DB::beginTransaction();
            $user   = User::query()->where('id', $user_id)->firstOrFail();
            $user->delete();
            DB::commit();

            return redirect()->back()->with(self::warning('Data has been deleted!'));
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(self::danger('Data cannot deleted from database!'));
        }
    }

    public function getUser(Request $request)
    {
        $keyword = $request->get('q');
        $users = User::where('name', 'like', "%$keyword%")->get();
        return $users;
    }
}
