<?php

namespace App\Providers;

use App\Traits\FlashMessage;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{

    use FlashMessage;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.message', function($view) {
            $messages = self::messages();

            return $view->with('messages', $messages);
        });

        Schema::defaultStringLength(191);
    }
}
