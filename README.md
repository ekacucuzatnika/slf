## Installation
- Extract Archive
- Copy .env.example to .env : `sudo cp .env.example .env`
- Set your database in .env file :
  ```
  DB_DATABASE=your_db
  DB_USERNAME=your_username
  DB_PASSWORD=your_password_of_db
  ```
- Create Database and must be same with config on env file
- Run the command `composer install` to install all the dependencies
- Run the command `php artisan key:generate` to generate the new key
- Run the command `php artisan migrate`
- Run the command `php artisan db:seed`
- Run the command `php artisan storage:link`
- Run the command `php artisan serve`. This will open Eta app in your browser
- Run this link in your browser : `http://127.0.0.1:8000` or `http://localhost:8000`
- Login with this account :
  ```
  Email : admin@gmail.com
  Password : rahasia
  ```


## Description
