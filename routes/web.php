<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome2');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 * Users / Employee
 */
Route::prefix('user')->name('user.')->namespace('UserManagement')->group(function() {
    Route::delete('/{user_id}/destroy', 'UserController@destroy')->name('destroy');
    Route::put('/{user_id}/update', 'UserController@update')->name('update');
    Route::get('/{user_id}/edit', 'UserController@edit')->name('edit');
    Route::post('/store', 'UserController@store')->name('store');
    Route::get('/create', 'UserController@create')->name('create');
    Route::get('/get/user', 'UserController@getUser')->name('getUser');
    Route::get('/', 'UserController@index')->name('index');
});

/**
 * Uploads
 */
Route::prefix('upload')->name('upload.')->namespace('Data')->group(function() {
    Route::delete('/{upload_id}/destroy', 'UploadController@destroy')->name('destroy');
    Route::put('/{upload_id}/update', 'UploadController@update')->name('update');
    Route::get('/{upload_id}/edit', 'UploadController@edit')->name('edit');
    Route::post('/store', 'UploadController@store')->name('store');
    Route::get('/create', 'UploadController@create')->name('create');
    Route::get('/', 'UploadController@index')->name('index');
});
