@if (count($messages))
@foreach ($messages as $message)
<div class="alert alert-{{ $message['level'] }} alert-dismissible fade show" role="alert">
    <strong>Hello {{ Auth::user()->name }}!</strong> {!! $message['message'] !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach
@endif
