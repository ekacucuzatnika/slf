@extends('layouts.app')
@section('title', 'Data Register')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Register</div>

                <div class="card-body">
                    @include('partials.message')

                    <p>
                        <a href="{{ route('upload.create') }}" class="btn btn-success">Tambah Register</a>
                    </p>

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kode Register</th>
                                <th scope="col">Nama Surveyor</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Company</th>
                                <th scope="col">Perumahan</th>
                                <th scope="col">Jumlah Unit</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">File Resume</th>
                                <th scope="col">QR Code</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($uploads as $item => $upload)
                                <tr>
                                    <td>{{ $item + 1 }}</td>
                                    <td>{{ $upload->register }}</td>
                                    <td>{{ $upload->user->name }}</td>
                                    <td>{{ date('d-m-Y', strtotime($upload->date)) }}</td>
                                    <td>{{ $upload->company }}</td>
                                    <td>{{ $upload->perumahan }}</td>
                                    <td>{{ $upload->unit_total}}</td>
                                    <td>{{ $upload->ket}}</td>
                                    <td><a href="{{ asset('storage'. $upload->file) }}" target="_blank">{{ basename('storage'.$upload->file) }}</a></td>
                                    <td><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('https://i.pinimg.com/originals/38/47/25/384725cbdf156d2f76ff17b1a14f93dd.png', 0.3, true)
                                        ->size(180)->errorCorrection('H')
                                        ->generate(asset('storage' .$upload->file))) !!} "></td>
                                    <td>
                                    <td>
                                        <a href="{{ route('upload.edit', ['upload_id' => $upload->id]) }}" class="btn btn-info btn-sm">Edit</a>
                                        <form method="POST" action="{{ route('upload.destroy', ['upload_id' => $upload->id]) }}"
                                            onsubmit="return confirm('Are you sure to delete this data of {{ $upload->register }}?')">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                              @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    {{ $uploads->links() }}
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
