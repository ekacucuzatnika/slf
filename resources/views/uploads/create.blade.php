@extends('layouts.app')
@section('title', 'Tambah Register')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Register</div>

                <div class="card-body">

                    @include('partials.message')

                    <form class="needs-validation" novalidate method="POST" action="{{ route('upload.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Tanggal</label>
                          <div class="col-sm-10">
                            <input type="text" name="date" class="form-control" id="date" required>
                            @error('date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Nama Surveyor</label>
                          <div class="col-sm-10">
                            <select class="form-control" style="width: 100%" id="user" name="user">
                            </select>
                            @error('user')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Company</label>
                          <div class="col-sm-10">
                            <input type="text" name="company" class="form-control" required>
                            @error('company')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Perumahan</label>
                            <div class="col-sm-10">
                              <input type="text" name="perumahan" class="form-control" placeholder="" required>
                              @error('company')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                              @enderror
                            </div>
                          </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jumlah Unit</label>
                            <div class="col-sm-10">
                              <input type="text" name="unit_total" onkeyup="validNumber(this)" class="form-control" required>
                              @error('unit_total')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                              @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Keterangan</label>
                            <div class="col-sm-10">
                              <input type="text" name="ket" class="form-control" placeholder="Jika kosong inputkan ' - '" required>
                              @error('company')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                              @enderror
                            </div>
                          </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">File Resume</label>
                            <div class="col-sm-10">
                              <input type="file" name="file" class="form-control" accept="application/pdf" required>
                              @error('file')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                              @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-sm-10">
                              <button type="submit" class="btn btn-success">Add</button>
                              <a href="{{ route('upload.index') }}" class="btn btn-secondary">Back</a>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
function validNumber(num)
{
    if(!/^[0-9.]+$/.test(num.value))
    {
        num.value = num.value.substring(0, num.value.length-1000);
    }
}

(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$('#date').flatpickr({
    dateFormat: "Y-m-d",
})

$('#user').select2({
    ajax: {
        url:  '{{ route('user.getUser') }}',
        processResults: function (data) {
            return {
                results: data.map(function (item) {
                    return {
                        id: item.id,
                        text: item.name
                    }
                })
            }
        }
    }
});
</script>
@endpush
