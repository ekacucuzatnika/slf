<!DOCTYPE html>
<html lang="en">
<head>
  <title>PT. Agnia Pratama Konsulindo</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style>
  .fakeimg {
    height: 200px;
    background: #aaa;
  }
  </style>
</head>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="/">
        <img src="https://i.pinimg.com/originals/38/47/25/384725cbdf156d2f76ff17b1a14f93dd.png" width="30" height="30" class="d-inline-block align-top" alt="">
        Agnia Pratama Konsulindo
      </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/login">Login</a>
      </li>
    </ul>
  </div>
</nav>

<div class="container" style="margin-top:30px">
      <div class="col-sm-15">
      <h2>Selamat Datang di Website PT. Agnia Pratama Konsulindo.</h2>
      <br>
      <p>Anda dapat melakukan pengajuan Sertifikat Laik Fungsi (SLF) Bangunan dengan menghubungi kami atau datang ke kantor kami.
      <br>
      <div class="alert alert-success">
      Jam kerja kami, Senin - Jum'at : 08:00 - 16:00 WIB </div></p>

      <h4><strong>PT. Agnia Pratama Konsulindo</strong></h4>
      <br>
      <p><strong>Office</strong> : Grand Cicantel 2 Kav. 61 Mulyasari Tamansari Tasikmalaya
      <br>
      <p><strong>Studio</strong> :
      <br>
      <ol>
        <li>Jl. Anyelir No.23 Kedung Jaya Kec. Kedawung Cirebon</li>
        <li>Jl. Graha Persada Kav. D.01 Sindangkasih Cikoneng Ciamis</li>
        <li>Jl. Veteran Gg. SMP 2 No. 8 Ciseureuh Purwakarta</li>
      </ol>
      <br>
      <strong>Telepon </strong> : 02179170107
      <br>
      <strong>Email </strong> : agniapratamakonsulindo93@gmail.com
      </p>
      <br>
      <a href="/login" class="btn btn-warning" role="button">Login Sebagai Karyawan</a>
  </div>
</div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="jumbotron text-center" style="margin-bottom:0">
  <p>2020 © Aplikasi E-Register Eka Cucu Zatnika</p>
</div>

</body>
</html>
