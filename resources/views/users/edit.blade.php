@extends('layouts.app')

@section('title', 'Edit Karyawan')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Karyawan</div>

                <div class="card-body">

                    @include('partials.message')

                    <form class="needs-validation" novalidate method="POST" action="{{ route('user.update', ['user_id' => $user->id]) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">NIK</label>
                          <div class="col-sm-10">
                            <input type="text" name="nik" value="{{ $user->nik }}" onkeyup="validNumber(this)" class="form-control" required>
                            @error('nik')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Name</label>
                          <div class="col-sm-10">
                            <input type="text" name="name" value="{{ $user->name }}" class="form-control" required>
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Email</label>
                          <div class="col-sm-10">
                            <input type="email" name="email" value="{{ $user->email }}" class="form-control" required>
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. HP</label>
                            <div class="col-sm-10">
                              <input type="text" name="phone" value="{{ $user->phone }}" onkeyup="validNumber(this)" class="form-control" required>
                              @error('phone')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                              @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Address</label>
                            <div class="col-sm-10">
                              <textarea name="address" class="form-control" rows="3" required>{{ $user->address }}</textarea>
                              @error('address')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                              @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-sm-10">
                              <button type="submit" class="btn btn-success">Save</button>
                              <a href="{{ route('user.index') }}" class="btn btn-secondary">Back</a>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
function validNumber(num)
{
    if(!/^[0-9.]+$/.test(num.value))
    {
        num.value = num.value.substring(0, num.value.length-1000);
    }
}

(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
@endpush
