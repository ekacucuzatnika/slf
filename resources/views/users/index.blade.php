@extends('layouts.app')

@section('title', 'Karyawan')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Karyawan</div>

                <div class="card-body">
                    @include('partials.message')

                    <p>
                        <a href="{{ route('user.create') }}" class="btn btn-success">Tambah Karyawan</a>
                    </p>

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">NIK</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">No. HP</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($users as $item => $user)
                                  <tr>
                                      <td>{{ $item + 1 }}</td>
                                      <td>{{ $user->nik }}</td>
                                      <td>{{ $user->name }}</td>
                                      <td>{{ $user->email }}</td>
                                      <td>{{ $user->phone }}</td>
                                      <td>{{ $user->address }}</td>
                                      <td>
                                        <a href="{{ route('user.edit', ['user_id' => $user->id]) }}" class="btn btn-info btn-sm">Edit</a>
                                        <form method="POST" action="{{ route('user.destroy', ['user_id' => $user->id]) }}"
                                            onsubmit="return confirm('Are you sure to delete this user of {{ $user->name }}?')">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                                        </form>
                                      </td>
                                  </tr>
                              @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="10">{{ $users->links() }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
