<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nik'       => '1234567890',
            'name'      => 'John Doe',
            'email'     => 'admin@gmail.com',
            'phone'     => '089878767',
            'password'  => Hash::make('rahasia'),
            'address'   => 'Bandung, Jawa Barat'
        ]);
    }
}
